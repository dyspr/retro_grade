var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.075
var steps = 4
var dimension = 11
var array = create2DArray(dimension, dimension, 0, false)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      for (var k = 0; k < array[i][j].length; k++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
        fill(sin(i * j + frameCount * 0.05 + asin(array[i][j][k]) * Math.PI) * 128 + 128)
        noStroke()
        rect(0, 0, boardSize * initSize * 0.9 * (1 - k / steps), boardSize * initSize * 0.9 * (1 - k / steps), boardSize * initSize * 0.9 * (1 - k / steps) * 0.25 * abs(sin((i + j) * 3 + frameCount * 0.01)))
        pop()
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        var rand = []
        for (var k = 0; k < steps; k++) {
          rand.push(Math.random())
        }
        columns[j] = rand
      }
    }
    array[i] = columns
  }
  return array
}
